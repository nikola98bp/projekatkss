const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const answerSchema = new Schema({
  userID: { type: Schema.Types.ObjectId, ref: "User" },
  questionID: String,
  odgovor: String,
  datetime: Date
});

module.exports = mongoose.model("Answer", answerSchema, "Answer");
