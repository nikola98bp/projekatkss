import QuestionApiService from "./QuestionApiService";
import AnswerApiService from "./AnswerApiService";
import UserApiService from "./UserApiService";
import LoginApiService from "./LoginApiService";

export default {
    question: new QuestionApiService(),
    answer: new AnswerApiService(),
    user: new UserApiService(),
    login: new LoginApiService()
}