import baseUrlWithoutForwardSlash from "./config/baseUrl";
import axios from "axios";

export default class LoginApiService {
    axios;

    constructor() {
        this.axios = axios;
    }

    post(loginDto) {
        return this.axios.post(`${baseUrlWithoutForwardSlash}/login`,{...loginDto});
    }

}